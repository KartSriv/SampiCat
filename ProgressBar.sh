#!/bin/sh
(
echo "10" ; sleep 1
echo "# Accesing Backups" ;
echo "20" ; sleep 1
echo "# Resetting cron jobs" ; sleep 1
echo "40" ; sleep 1
echo "# Removing all files from SampiCat" ; sleep 1
echo "75" ;
echo "" ; sleep 1
echo "100" ; sleep 1
) |
zenity --progress \
  --title="SampiCat" \
  --text="Reseting SampiCat" \
  --percentage=0

if [ "$?" = -1 ] ; then
        zenity --error \
          --text="Update canceled."
fi
