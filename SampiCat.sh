#!/bin/bash

# Reference: https://www.techrepublic.com/blog/linux-and-open-source/gui-scripting-with-zenity/
python InitialCode.py
zenity --question --text "Do you want to clear SampiCat's Cashe (Basically Trash)? \n NOTE: By clearing SampiCat's Cashe you'll save space from your HDD." --ok-label "Hell Yeah!" --cancel-label="No Please :("

rc=$?
if [ "${rc}" == "1" ]; then
   notify-send "SampiCat's Data has been planted on the lands of computer. It'll grow soon :("
else
    usrname=$(whoami) # Username
    cd /home/$usrname/SampiCat # SampiCat Directory
    python ResetSampiCat.py
    ./ProgressBar.sh
    notify-send "SampiCat's Data has been decomposed on the lands of computer :)"
fi
